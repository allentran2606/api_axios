const BASE_URL = "https://635f4b20ca0fe3c21a991e72.mockapi.io";

let idEdited = null;

// Axios - Render all todos service
function fetchAllToDo() {
  turnOnLoading();
  axios({
    url: `${BASE_URL}/todos`,
    method: "GET",
  })
    .then(function (res) {
      turnOffLoading();
      renderToDoList(res.data);
    })
    .catch(function (err) {
      turnOffLoading();
      console.log(`err: `, err);
    });
}
fetchAllToDo();

// Remove todos service
function removeTodo(idTodo) {
  turnOnLoading();
  axios({
    url: `${BASE_URL}/todos/${idTodo}`,
    method: "DELETE",
  })
    .then(function (res) {
      turnOffLoading();
      fetchAllToDo();
    })
    .catch(function (err) {
      turnOffLoading();
      console.log(`err: `, err);
    });
}

// Add todos
function addTodo() {
  var data = layThongTinTuForm();
  let newTodo = {
    name: data.name,
    desc: data.desc,
    isComplete: true,
  };
  axios({
    url: `${BASE_URL}/todos`,
    method: "POST",
    data: newTodo,
  })
    .then(function (res) {
      turnOffLoading();
      fetchAllToDo();
    })
    .catch(function (err) {
      turnOffLoading();
      console.log(`err: `, err);
    });
}
/* KIẾN THỨC BÊN NGOÀI
  promise chaining
  promise all
*/

function editTodo(idTodo) {
  turnOnLoading();
  axios({
    url: `${BASE_URL}/todos/${idTodo}`,
    method: "GET",
  })
    .then(function (res) {
      // Show thông tin lên form
      turnOffLoading();
      document.getElementById(`name`).value = res.data.name;
      document.getElementById(`desc`).value = res.data.desc;
      idEdited = res.data.id;
    })
    .catch(function (err) {
      turnOffLoading();
      console.log(`err: `, err);
    });
}

function updateTodo() {
  var data = layThongTinTuForm();
  var newTodo = {
    name: data.name,
    desc: data.desc,
    isComplete: true,
  };
  axios({
    url: `${BASE_URL}/todos/${idEdited}`,
    method: "PUT",
    data: data,
  })
    .then(function (res) {
      fetchAllToDo();
    })
    .catch(function (err) {
      console.log(`err: `, err);
    });
}
