function renderToDoList(todos) {
  let contentHTML = ``;
  todos.forEach(function (item) {
    let contentTr = `<tr>
        <td>${item.id}</td>
        <td>${item.name}</td>
        <td>${item.desc}</td>
        <td>
            <input type="checkbox" ${item.isComplete ? "checked" : ""}>
        </td>
        <td>
            <button onclick="removeTodo(${
              item.id
            })" class="btn btn-danger">Delete</button>

            <button onclick="editTodo(${
              item.id
            })" class="btn btn-secondary">Edit</button>
        </td>
    </tr>`;
    contentHTML += contentTr;
  });
  document.getElementById(`tbody-todos`).innerHTML = contentHTML;
}

function turnOnLoading() {
  document.getElementById(`loading`).style.display = `flex`;
}

function turnOffLoading() {
  document.getElementById(`loading`).style.display = `none`;
}

function layThongTinTuForm() {
  let name = document.getElementById(`name`).value;
  let desc = document.getElementById(`desc`).value;
  return {
    name: name,
    desc: desc,
  };
}
